This is an example repo for the GitHub action: https://github.com/marketplace/actions/gitlab-pipeline-trigger

The GitHub action will trigger pipelines on this repo, wait for their completion and report back the pipeline status.
